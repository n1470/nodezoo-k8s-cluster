# Nodezoo Kubernetes Cluster Creator 

## Development cluster

#### Set up a minikube cluster
```shell
minikube -p nodezoo start --driver=virtualbox --memory=8192 --cpus=4
istioctl install --set profile=demo -y
kubectl create namespace nodezoo
kubectl label namespace nodezoo istio-injection=enabled
kubectl apply -f samples/addons #from within istio download
make local_deploy
```

Then bring up all Kubernetes services in the ``nodezoo`` namespace such as ``nodezoo-npm-service``
```shell
make dev_deploy
```

Once all services are up and running, the istio gateway is ready to take requests.
```shell
minikube -p nodezoo tunnel # in a separate console
kubectl get svc istio-ingressgateway -n istio-system # make a note of the external ip, example: 10.106.231.214
```
```shell
NAME                   TYPE           CLUSTER-IP       EXTERNAL-IP      PORT(S)
istio-ingressgateway   LoadBalancer   10.106.231.214   10.106.231.214   15021:30565/TCP...
```

#### Test cluster
```shell
istioctl dashboard kiali # in another console, opens the browser window
curl -v http://10.106.231.214/info\?q\=express
```
Watch the traffic trace on the Kiali dashboard.

#### Logs
```shell
minikube -p nodezoo ip # 192.168.59.100
kubectl get services -n logging
```

Note the ``kibana`` port in the services output.
```shell
NAME            TYPE       CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
elasticsearch   NodePort   10.97.76.221    <none>        9200:30682/TCP   4d9h
kibana          NodePort   10.99.138.108   <none>        5601:30241/TCP   4d9h
```
- Open browser to the minikube IP and ``kibana`` node port (30241 in this example) 
  http://192.168.59.100:30241/app/kibana#/home

- Go the **Management** section in the sidebar. Create an index pattern of ``logstash*``, go to the
  **Discover** section and hit _Refresh_.

Logs should now be visible.
#### Stop cluster
```shell
minikube -p nodezoo stop
```

### EKS

#### Master role
A master role is needed to connect to EKS with `kubectl`.

Create a role with trust relationship with the principal (user) used for creating the cluster.
```shell
export PRINCIPAL_AWS=$(aws --profile cdk sts get-caller-identity | jq -r '.Arn')
cat <<-EOF > /tmp/trust_policy.json 
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "Statement1",
            "Effect": "Allow",
            "Principal": {
                "AWS": "$PRINCIPAL_AWS"
            },
            "Action": "sts:AssumeRole"
        }
    ]
}
EOF

export MASTER_ROLE_ARN=$(aws --profile cdk iam create-role \
--path /nodezoo/ \
--role-name eksMaster \
--assume-role-policy-document "$(jq -c '.' /tmp/trust_policy.json | jq -r '.Role.Arn')")
```

If a role was created earlier, fetch the ARN.
```shell
export MASTER_ROLE_ARN=$(aws --profile cdk iam list-roles \
--path-prefix /nodezoo | jq -r '.Roles[] | select(.RoleName == "eksMaster") | .Arn')
```

#### Bootstrap
For one time or unless `CDKToolkit` stack does not exist.
```shell
make dev_bootstrap
```

#### Start
```shell
make dev_dist dev_deploy
```

#### Stop

```shell
AWS_PROFILE=cdk make delete_stack
```
This may take several minutes. This step may also not be able to delete
all resources; a manual check is needed if the reported status is FAILED at the end.

## Production Cluster

The `MASTER_ROLE_ARN` environment variable must be defined. See **Master Role** section above.

```shell
make dist deploy
```

Enjoy!
