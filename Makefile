# Targets for local development
.phony: clean
clean:
	@./gradlew clean --console=plain && rm -rf cdk.out

.phony: test
test:
	@./gradlew test --console=plain

.phony: dev_dist
dev_dist:
	@if [ -n "$${MASTER_ROLE_ARN}" ]; then \
		cdk synth; \
	else \
		echo "[Error]: MASTER_ROLE_ARN environment variable is not set"; \
		exit 1; \
	fi

.phony: dev_bootstrap
dev_bootstrap:
	@cdk --profile cdk bootstrap

.phony: dev_deploy
dev_deploy:
	@if [ -n "$${MASTER_ROLE_ARN}" ]; then \
		cdk --profile cdk deploy; \
	else \
		echo "[Error]: MASTER_ROLE_ARN environment variable is not set"; \
		exit 1; \
	fi

.phony: local_deploy
local_deploy:
	@kubectl get ns logging 1>/dev/null 2>&1 || kubectl create ns logging
	@kubectl apply -f etc/kubernetes/elastic.yaml -n logging
	@kubectl apply -f etc/kubernetes/kibana.yaml -n logging
	@kubectl apply -f etc/kubernetes/fluentd-rbac.yaml
	@kubectl apply -f etc/kubernetes/fluentd-daemonset.yaml

.phony: dist
dist:
	@cdk synth

.phony: bootstrap
bootstrap:
	@cdk bootstrap

.phony: deploy
deploy:
	@set -ev
	@if [ -z "$${MASTER_ROLE_ARN}" ]; then \
  	  echo "[Error]: MASTER_ROLE_ARN environment variable is not set"; \
  	  exit 1; \
  	fi
	@cdk --require-approval=never --ci=true deploy


ifdef ($(AWS_PROFILE))
AWS_CMD := "aws --profile $(AWS_PROFILE)"
else
AWS_CMD := "aws"
endif

.phony: delete_stack
delete_stack:
	@set -ev
	@for lb_arn in $$(${AWS_CMD} elbv2 describe-load-balancers | jq -r '.LoadBalancers[].LoadBalancerArn'); do \
  		echo "deleting $${lb_arn}"; \
    	${AWS_CMD} elbv2 delete-load-balancer --load-balancer-arn $${lb_arn}; \
    done; \
    ${AWS_CMD} cloudformation delete-stack --stack-name NodezooK8SClusterStack


# Targets for complete development and testing in Docker
PROJECT_NAME := nodezoo-k8s-cluster
PROJECT_SLUG := n1470/${PROJECT_NAME}
GITLAB_REPO := registry.gitlab.com

.phony: ci_image
ci_image:
	@docker build --build-arg PROJECT_PATH=${PROJECT_SLUG} -t ${GITLAB_REPO}/${PROJECT_SLUG} .

.phony: ci_run
ci_run:
	@docker run \
	-it \
	--rm \
	-d \
	--name ${PROJECT_NAME}-app \
	${GITLAB_REPO}/${PROJECT_SLUG}:latest \
	bash

.phony: ci_stop
ci_stop:
	@docker stop ${PROJECT_NAME}-app

.phony: ci_cli
ci_cli:
	@docker exec \
	-it \
	${PROJECT_NAME}-app \
	bash

.phony: ci_copy
ci_copy:
	@docker cp . ${PROJECT_NAME}-app:/builds/n1470/${PROJECT_NAME}

.phony: ci_clean
ci_clean:
	@docker exec \
	-it \
	${PROJECT_NAME}-app \
	bash -c "make clean"

.phony: ci_test
ci_test:
	@docker exec \
	-it \
	${PROJECT_NAME}-app \
	bash -c "make test"

.phony: ci_dist
ci_dist:
	@docker exec \
	-it \
	${PROJECT_NAME}-app \
	bash -c "make dist"

.phony: ci_bootstrap
ci_bootstrap:
	@docker exec \
	-it \
	-e AWS_ACCESS_KEY_ID=$$AWS_ACCESS_KEY_ID \
	-e AWS_SECRET_ACCESS_KEY=$$AWS_SECRET_ACCESS_KEY \
	-e AWS_DEFAULT_REGION=$$AWS_DEFAULT_REGION \
	${PROJECT_NAME}-app \
	bash -c "make bootstrap"

.phony: ci_deploy
ci_deploy:
	@docker exec \
	-it \
	-e AWS_ACCESS_KEY_ID=$$AWS_ACCESS_KEY_ID \
	-e AWS_SECRET_ACCESS_KEY=$$AWS_SECRET_ACCESS_KEY \
	-e AWS_DEFAULT_REGION=$$AWS_DEFAULT_REGION \
	${PROJECT_NAME}-app \
	bash -c "make deploy"
