package tao.nodezoo.eks;

import org.jetbrains.annotations.NotNull;
import software.amazon.awscdk.Stack;
import software.amazon.awscdk.services.eks.CfnAddon;

public class EksAddonManager {

    private final Stack stack;
    private final String clusterName;

    public record EksAddon(
            @NotNull String id,
            @NotNull String addonName,
            @NotNull String addonVersion
    ) { }

    public static class EksAddonBuilder {
        private final String addonName;
        private String addonVersion;

        public EksAddonBuilder(@NotNull String addonName) {
            this.addonName = addonName;
        }

        public EksAddonBuilder addonVersion(@NotNull String addonVersion) {
            this.addonVersion = addonVersion;
            return this;
        }

        public EksAddon build() {
            return new EksAddon("nodezoo-" + this.addonName, this.addonName, this.addonVersion);
        }
    }

    public EksAddonManager(Stack stack, @NotNull String clusterName) {
        this.stack = stack;
        this.clusterName = clusterName;
    }

    public EksAddonManager createAddOn(@NotNull EksAddon eksAddOn) {
        CfnAddon.Builder.create(this.stack, eksAddOn.id())
                .addonName(eksAddOn.addonName())
                .addonVersion(eksAddOn.addonVersion())
                .clusterName(this.clusterName)
                .build();

        return this;
    }
}
