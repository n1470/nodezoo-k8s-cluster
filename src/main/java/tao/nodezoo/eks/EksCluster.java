package tao.nodezoo.eks;

import org.jetbrains.annotations.NotNull;
import software.amazon.awscdk.cdk.lambdalayer.kubectl.v23.KubectlV23Layer;
import software.amazon.awscdk.services.eks.*;
import software.amazon.awscdk.services.iam.Role;
import software.constructs.Construct;

import java.util.List;

public class EksCluster {

    public static final int DEFAULT_CLUSTER_CAPACITY = 3;
    public static final String DEFAULT_CLUSTER_ID = "nodezoo-eks";
    private final Construct scope;
    private final int capacity;
    private final String clusterId;
    private Cluster cluster;
    private String masterRoleArn;

    public EksCluster(@NotNull Construct scope) {
        this(scope, DEFAULT_CLUSTER_CAPACITY, DEFAULT_CLUSTER_ID);
    }

    public EksCluster(@NotNull Construct scope, int capacity, String clusterId) {
        this.scope = scope;
        this.capacity = capacity;
        this.clusterId = clusterId;
    }

    public EksCluster create() {
        var clusterBuilder = Cluster.Builder
                .create(scope, clusterId)
                .version(KubernetesVersion.V1_26)
                .kubectlLayer(new KubectlV23Layer(scope, clusterId + "kubectlLayer"))
                .defaultCapacity(capacity)
                .albController(buildAlbController())
                .clusterLogging(clusterLoggingTypes())
                .outputClusterName(true)
                .outputConfigCommand(true)
                .outputMastersRoleArn(true);

        if (masterRoleArn != null) {
            clusterBuilder.mastersRole(Role.fromRoleArn(scope, "masters", masterRoleArn));
        }

        this.cluster = clusterBuilder.build();
        return this;
    }

    public String clusterName() {
        return this.cluster.getClusterName();
    }

    public EksCluster withMasterRoleArn(String arn) {
        this.masterRoleArn = arn;
        return this;
    }

    @NotNull
    private AlbControllerOptions buildAlbController() {
        return AlbControllerOptions.builder()
                .version(AlbControllerVersion.V2_5_1)
                .build();
    }

    private List<ClusterLoggingTypes> clusterLoggingTypes() {
        return List.of(ClusterLoggingTypes.API, ClusterLoggingTypes.AUTHENTICATOR, ClusterLoggingTypes.SCHEDULER);
    }
}
