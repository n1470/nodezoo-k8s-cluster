package tao.nodezoo;

import software.amazon.awscdk.StackProps;
import software.constructs.Construct;
import software.amazon.awscdk.Stack;
import tao.nodezoo.eks.EksAddonManager;
import tao.nodezoo.eks.EksCluster;

public class NodezooK8SClusterStack extends Stack {
    public NodezooK8SClusterStack(final Construct scope, final String id) {
        this(scope, id, new NodezooK8SClusterStackProps(StackProps.builder().build()));
    }

    public NodezooK8SClusterStack(final Construct scope, final String id, final NodezooK8SClusterStackProps props) {
        super(scope, id, props.getStackProps());
        final var cluster = new EksCluster(this)
                .withMasterRoleArn(props.getMasterRoleArn())
                .create();
        createAddons(cluster);
    }

    private void createAddons(EksCluster cluster) {
        final var kubeProxyAddon = new EksAddonManager.EksAddonBuilder("kube-proxy")
                .build();
        final var vpCniAddon = new EksAddonManager.EksAddonBuilder("vpc-cni")
                .build();
        final var coreDnsAddon = new EksAddonManager.EksAddonBuilder("coredns")
                .build();
        new EksAddonManager(this, cluster.clusterName())
                .createAddOn(kubeProxyAddon)
                .createAddOn(vpCniAddon)
                .createAddOn(coreDnsAddon);
    }
}
