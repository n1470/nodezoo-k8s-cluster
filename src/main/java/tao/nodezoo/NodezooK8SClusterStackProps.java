package tao.nodezoo;

import software.amazon.awscdk.StackProps;

public class NodezooK8SClusterStackProps {

    private final StackProps stackProps;
    private String masterRoleArn;

    public NodezooK8SClusterStackProps(StackProps stackProps) {
        this.stackProps = stackProps;
    }

    public String getMasterRoleArn() {
        return masterRoleArn;
    }

    public void setMasterRoleArn(String masterRoleArn) {
        this.masterRoleArn = masterRoleArn;
    }

    public StackProps getStackProps() {
        return stackProps;
    }
}
