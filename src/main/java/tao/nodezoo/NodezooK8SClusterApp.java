package tao.nodezoo;

import org.apache.commons.cli.*;
import software.amazon.awscdk.App;
import software.amazon.awscdk.StackProps;

public class NodezooK8SClusterApp {
    public static void main(final String[] args) throws ParseException {
        var app = new App();
        // If you don't specify 'env', this stack will be environment-agnostic.
        // Account/Region-dependent features and context lookups will not work,
        // but a single synthesized template can be deployed anywhere.

        // Uncomment the next block to specialize this stack for the AWS Account
        // and Region that are implied by the current CLI configuration.
                /*
                .env(Environment.builder()
                        .account(System.getenv("CDK_DEFAULT_ACCOUNT"))
                        .region(System.getenv("CDK_DEFAULT_REGION"))
                        .build())
                */

        // Uncomment the next block if you know exactly what Account and Region you
        // want to deploy the stack to.
                /*
                .env(Environment.builder()
                        .account("123456789012")
                        .region("us-east-1")
                        .build())
                */

        // For more information, see https://docs.aws.amazon.com/cdk/latest/guide/environments.html
        var props = new NodezooK8SClusterStackProps(StackProps.builder().build());
        var cmdParser = new DefaultParser();
        var cmd = cmdParser.parse(getOptions(), args);
        if (cmd.hasOption("r")) {
            props.setMasterRoleArn(cmd.getOptionValue("r"));
        }

        new NodezooK8SClusterStack(app, "NodezooK8SClusterStack", props);
        app.synth();
    }

    private static Options getOptions() {
        var options = new Options();
        options.addOption(new Option("r", true, "masterRoleArn"));
        return options;
    }
}

