 package tao.nodezoo;

 import software.amazon.awscdk.App;
 import software.amazon.awscdk.assertions.Template;
 import java.io.IOException;

 import java.util.HashMap;
 import java.util.Map;

 import org.junit.jupiter.api.Test;
 import tao.nodezoo.eks.EksCluster;

 public class NodezooK8SClusterTest {

     @Test
     public void testStack() {
         App app = new App();
         NodezooK8SClusterStack stack = new NodezooK8SClusterStack(app, "test");

         Template template = Template.fromStack(stack);
         template.hasResource("AWS::EKS::Nodegroup",
                 Map.of("Properties",
                         Map.of("ScalingConfig",
                                 Map.of("DesiredSize", EksCluster.DEFAULT_CLUSTER_CAPACITY))));
     }
 }
