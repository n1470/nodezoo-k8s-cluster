FROM openjdk:17.0.2-bullseye

ARG PROJECT_PATH

RUN apt-get update && \
    apt-get -y upgrade && \
    curl -fsSL https://deb.nodesource.com/setup_16.x | bash - && \
    apt-get install -y nodejs zip libarchive-tools make && \
    rm -rf /var/lib/apt/lists/*

RUN npm install -g aws-cdk@2.85.0

RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
    unzip awscliv2.zip && \
    ./aws/install && \
    rm -rf ./aws awscliv2.zip

WORKDIR /builds/$PROJECT_PATH

COPY *.gradle gradlew.* gradlew /builds/$PROJECT_PATH/
COPY gradle/ /builds/$PROJECT_PATH/gradle/
RUN ./gradlew build -x test --no-daemon --continue --parallel || true

COPY Makefile /builds/$PROJECT_PATH/

CMD ["bash"]
